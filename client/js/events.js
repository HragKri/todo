"use strict";

Template.form.events({
    'submit .add-new-task': function (event) {
        event.preventDefault();

        var taskName = event.currentTarget.children[0].firstElementChild.value;
        Collections.Todo.insert({
            name: taskName,
            createdAt: new Data(),
            complete: false
        });

        return false;
    }
});

Templae.todos.events({
    'click .delete-task': function (event) {
        Collections.Todo.remove({_id: this._id});
    },
        'click .complete-task': function (event) {
        Collections.Todo.update({_id: this._id}, {$set: {complete: true}});
        }
});